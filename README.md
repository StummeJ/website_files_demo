# How to contribute:
## General Flow
1. create a fork
1. Make changes to data.json  
1. create pull request of the local fork to pilot  
1. once approved - Jenkins will deploy to [https://pilot.secdsm.org/](https://pilot.secdsm.org)
1. validate changes to pilot
1. repeat until pilot looks the way it should
1. create PR from pilot to master.

##  Merge Access to Pilot
The idea is that you should be able to approve your own merge requests to pilot.secdsm.org  
Hit up zoomequipd in slack for merge access to pilot

## The deployment process
When a PR is approved to either pilot or master the following process kicks off  

1. Jenkins gets a job to build website_files
1. Jenkins checks that the data.json files is able to be parsed by running "check-json.rb"
    * if the check exits non-zero the build fails
1. Jenkins runs the render.rb script
    * if the script exits non-zero the build fails
1. The previous version of the site is deleted  - /var/www/html/[pilot/prod]
1. The current version of the site is copied over to /var/www/html/[pilot/prod]
1. If there is a index_compiled.html file, it is moved to /var/www/html/[pilot/prod]/index.html
    * if there is not a index_compiled.html the build fails
1. Files are removed from /var/www/html/[pilot/prod]/
    * .git
    * data.json
    * template.html
    * render.rb
    * check-json.rb
    * .gitignore
1. The build status is then reported back to bitbucket.


---------------------------------------
# index.html details
This file contains a default "maintance" style page. In the event that a deployment process, this serves as a catchall to ensure a page is still available.   
Generally speaking, anytime ["template.html"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-template.html) is updated, this page will also need to be updated  

# template.html details
This file contains the shell that is used by the ["render.rb"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-render.rb) script to build out ["index_compiled.html"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-index_compiled.html)

# render.rb 
Ruby script that depends on the ["Liquid"](https://shopify.github.io/liquid/) and Json gems.   
We use Liquid to build out index_compiled.html based on the conten of the ["data.json"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-data.json)  

# index_compiled.html
This file is created by the ["render.rb"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-render.rb) script.   
It file gets moved after being compiled to /var/www/html/[prod|pilot]/index.html (overwriting the default "maintance" style page)  
this file should never be committed to git and is included in the .gitignore file

# data.json details
data.json controls all the elements that change month to month. Adding a talk/event/etc all happens here.  
This is the main file that should be getting modified.  


Must be a json formatted file - this controls all the dynamic fields for the [secdsm.org](https://secdsm.org) page  
There are two main sections to the data.json file

  1. [schedules](https://bitbucket.org/secdsm/website_files/overview#markdown-header-schedules)
  1. [events](https://bitbucket.org/secdsm/website_files/overview#markdown-header-events)

## Schedules
an array of hashes containing the details for the schedule section of the website. 

* Each element in the array should contain a full month's worth of details.
* this includes an ["intro"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-intro-talks), ["tooltalks"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-tool-talks)", and ["features"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-feature-talks) elements- see below for details on those keys.

### Keys Available to the schedule
* "alert"
* "date"
* ["sponsor"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-sponsor) - a hash containing information about sponsors
* ["intro"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-intro-talks) - a hash containing the intro talk details
* ["tooltalks"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-tool-talks) - an array containing tools talks
* ["features"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-feature-talks) - an array containing feature talks

##### Sponsor
###### Keys Available to elements in sponsor hash
  * "sponsor" - Name of the sponsor
  * "image" - Name of the image in the images/sponsorlogos/ directory 

##### Intro Talks
###### Keys Available to elements in introtalks hash
  * "title"
  * "time"
  * "description"

##### Tool Talks
###### Keys Available to elements in tooltalks array
  * "title"
  * "description" - required
  * "time"
  * "speaker"  - an array containing speaker details
    * "name"
    * "bio" - optional

##### Feature Talks
###### Keys Available to elements in features arrays
  * "title"
  * "description" - optional, but generally desired
  * "time"
  * "speaker"  - an array containing speaker details
    * "name"
    * "bio" - optional

## Events
an array of hashes containing the details for the events section of the website.

* Each element in the array should contain a full month's worth of events.  
* Each element in the "events" array becomes a collapsible box 
* Order matters when adding months to the events section.  
  * The first element will be the element that is expanded.
  * Months are ascending

### Keys Available to elements in events arrays
  * "url"
  * "name"
  * "time"
  * "description" - optional
  * "geolocation"  - optional  
    * You can embed a google maps by :  
    
        1. Go to http://maps.google.com  
        1. Select a point of interest  
        1. Click share  
        1. Click the `Embed map` tab  
        1. Copy the link in the `src` attribute  